![WideImg](http://www.houston-pc.com/wp-content/uploads/2016/05/Custom-Coding-Houston-TX-Houston-PC-Services.png)

# [→ Curso de diseño web 2021]
### HTML, CSS y Javascript

Utilizaremos Javascript, desde las bases pero haciendo hincapié en las nuevas características de ECMAScript6. Por cada tema habrá ejercicios puntuales y/o desafíos (Challenges). Los desafíos son proyectos completos que formarán parte del portfolio del alumno. Desde el primer mes se construirá el portfolio que será online, requisito necesario para poder conseguir trabajo.

Información sobre el Curso
=================

Las clases en vivo será los días viernes a las 17.00, con la siguiente propuesta de trabajo:

- Se repasará los temas de la semana de manera resumida
- Despejar dudas de los ejercicios
- Avance de la próxima semana
- Durante la semana se podrán hacer consultas usando slack. Slack es como whatsapp pero para trabajar.

Link en el campus del [Centro 27](http://centro27.edu.ar/)

[Clases grabadas]
=================
Fecha | Clase (Youtube) | Recursos
------------ | ---------- | ------------
31/03/2021 | [Clase 1](https://www.youtube.com/watch?v=pF4IS7Ftg_U&list=PLirPTHU5kzhFJLdK4a5DII-J0Q64kmoqc&index=1&t=6008s) | recurso
09/04/2021 | [Clase 2](https://www.youtube.com/watch?v=wv2L8gmX2BU&list=PLirPTHU5kzhFJLdK4a5DII-J0Q64kmoqc&index=9) | [LinkedInd](https://www.linkedin.com/) 
16/04/2021 | [Clase 3](https://www.youtube.com/watch?v=kkPmMb3Q-5w&list=PLirPTHU5kzhFJLdK4a5DII-J0Q64kmoqc&index=14&ab_channel=SergioGim%C3%A9nez) |  
23/04/2021 | [Clase 4](https://www.youtube.com/watch?v=1j-DEe77ohQ&list=PLirPTHU5kzhFJLdK4a5DII-J0Q64kmoqc&index=14) | 


[Programa de estudios]
=================

**HTML - Interfaz web**
- [Sintaxis](https://www.youtube.com/watch?v=J1N_CXOwyFI)
- [Página de ejemplo](https://www.youtube.com/watch?v=saNUADyCyLM)
- [Elementos en bloque y en línea](https://www.youtube.com/watch?v=C5U2qPYO7NE) 
- [Links y listas 1](https://www.youtube.com/watch?v=o0EnRSWS0YI)
- [Links y listas 2](https://www.youtube.com/watch?v=zkz5axdRK_4)
- [Contenido embebido](https://www.youtube.com/watch?v=wKXPlLnVH0M)
- [Tablas](https://www.youtube.com/watch?v=60wb-PZZQfk)
- [Formularios](https://www.youtube.com/watch?v=x7e5MFXUKU0)
- [HTML 5, tags]()
- Recursos:
    - [Presentación](https://www.dropbox.com/scl/fi/yig83y73dzgxzen3ay1ss/HTML.gslides?dl=0&rlkey=bpps6yfdsz3t12934errcce0c) 
    - [Códigos en el repositorio](https://gitlab.com/disenioweb2021/html)


**CSS**
- Selectores
- Modelo de caja

**Javascrcipt**
- Introducción: [Presentación](https://www.dropbox.com/preview/Work/CFP/2021/programador/JS/1.%20Introducci%C3%B3n%20-%20Javascript.gslides?role=personal)
- Tipos de datos: [Presentación](https://www.dropbox.com/scl/fi/fkwjncarnwefa1watyxp2/3.-Tipos-de-datos.gslides?dl=0&rlkey=x35kbry34nggxiughnmxo68tc)
- Variables: [Presentación](https://www.dropbox.com/scl/fi/3hww4c0417uv2sa07gj7g/4.-Variables.gslides?dl=0&rlkey=ysy9q5m9axxmpils8gz1jo1uk)
  
